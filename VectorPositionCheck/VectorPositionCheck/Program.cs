﻿using System;
using System.Numerics;

namespace VectorPositionCheck
{
    class Program
    {
        static void Main()
        {
            Vector2 dir = new Vector2(1f, 0f);
            Vector2 target = new Vector2(-500, -100);

            bool result = Result(dir, target);
            Console.WriteLine(result ? "точка находится слева или на векторе" : "точка лежит правее");
        }

        private static bool Result(Vector2 dir, Vector2 target)
        {
            float dot = Vector2.Dot(Vector2.Normalize(dir), Vector2.Normalize(target));
            return dot <= 0;
        }
    }
}
