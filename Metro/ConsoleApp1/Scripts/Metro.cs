﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleApp.Metro
{
    /// <summary>
    /// Реализация схемы метро
    /// </summary>
    public class MetroMap
    {
        public List<MetroLineInfo> MetroLines { private set; get; } = new List<MetroLineInfo>();

        /// <summary>
        /// Если под рукой нету схемы метро в формате json, то можно использовать это
        /// </summary>
        public static MetroMap CreateTestMetroMap()
        {
            MetroMap newMetroMap = new MetroMap();
            newMetroMap.CreateTestMetroLines();
            return newMetroMap;
        }

        /// <summary>
        /// Создание схемы метро из Json
        /// </summary>
        /// <param name="json">схема метро в Json</param>
        /// <param name="metroMap">объект после десериализации</param>
        public static bool TryGetMetroMapFromJson(string json, out MetroMap metroMap)
        {
            try
            {
                metroMap = JsonConvert.DeserializeObject<MetroMap>(json);
            }
            catch (JsonException)
            {
                Console.WriteLine("невалидный формат json");
                metroMap = null;
                return false;
            }

            return metroMap != null;
        }

        /// <summary>
        /// Вернет Json текущего экземпляра схемы метро
        /// </summary>
        public string GetJson()
        {
            return JsonConvert.SerializeObject(this);
        }

        /// <summary>
        /// Получение короткого пути по имени станции
        /// </summary>
        /// <param name="firstStatio">Имя первой станции</param>
        /// <param name="lastStation">Имя конечной станции</param>
        public IReadOnlyList<StationInfo> GetShortWay(string firstStatio, string lastStation)
        {
            if (!TryGetAllMetroLinesByStations(firstStatio, lastStation, out IReadOnlyList<MetroLineInfo> findedMetroLines))
            {
                Console.WriteLine("короткий путь не найден");
            }
            return GetShortestPath(findedMetroLines, firstStatio, lastStation);
        }

        private IReadOnlyList<StationInfo> GetShortestPath(IReadOnlyList<MetroLineInfo> metroLineInfos, string firstStatio, string lastStation)
        {
            List<StationInfo> stationWay = new List<StationInfo>();
            int firstIndex = 0;

            for (int i = 0; i < metroLineInfos.Count; i++)
            {
                var curMetroLineInfo = metroLineInfos[i];
                bool lineHasPath = curMetroLineInfo.IsContainStation(firstStatio) && curMetroLineInfo.IsContainStation(lastStation);
                List<StationInfo> stationsInLine = metroLineInfos[i].Stations.ToList();

                TryReverseNode(stationWay, stationsInLine, lastStation);
                var nextMetroLineInfo = i + 1 < metroLineInfos.Count ? metroLineInfos[i + 1] : null;
                TryAddValidStation(stationWay, stationsInLine, firstStatio, ref firstIndex, lineHasPath, nextMetroLineInfo);

                if (lineHasPath)
                {
                    break;
                }
            }

            return stationWay.GetRange(firstIndex, stationWay.Count - firstIndex);
        }

        private void TryAddValidStation(List<StationInfo> stationWay, List<StationInfo> stationsInLine, string firstStatio, ref int firstIndex, bool lineHasPath, MetroLineInfo nextMetroLineInfo)
        {         
            while (stationsInLine.Count != 0)
            {
                StationInfo station = stationsInLine[0];
                int stationWayCount = stationWay.Count;
                if (station.Name == firstStatio)
                {
                    firstIndex = stationWayCount;
                }
                if (stationWayCount == 0 || stationWay[stationWayCount - 1].Name != station.Name)
                {
                    stationWay.Add(station);
                }
                stationsInLine.Remove(station);

                if (nextMetroLineInfo != null)
                {
                    if (!lineHasPath && nextMetroLineInfo.IsContainStation(station.Name))
                    {
                        break;
                    }
                }
            }
        }

        private void TryReverseNode(List<StationInfo> stationWay, List<StationInfo> stationsInLine, string lastStation)
        {
            if (stationWay.Count > 0)
            {
                if (stationsInLine[0].Name != stationWay[stationWay.Count - 1].Name)
                {
                    stationsInLine.Reverse();
                }
            }
            else
            {
                if (stationsInLine[0].Name == lastStation)
                {
                    stationsInLine.Reverse();
                }
            }
        }


        private bool TryGetAllMetroLinesByStations(string firstStation, string lastStation, out IReadOnlyList<MetroLineInfo> metroLines)
        {
            List<MetroLineInfo> findedMetroLines = new List<MetroLineInfo>();

            foreach (MetroLineInfo metroLineInfo in MetroLines)
            {
                if (metroLineInfo.IsContainStations(firstStation, lastStation))
                {
                    findedMetroLines.Add(metroLineInfo);
                }
            }

            metroLines = findedMetroLines;
            return metroLines != null;
        }

        private void CreateTestMetroLines()
        {
            var testLine1 = new StationInfo[]
            {
                new StationInfo("H"),
                new StationInfo("G"),
                new StationInfo("F"),
                new StationInfo("E"),
            };
            var testLine2 = new StationInfo[]
            {
                new StationInfo("G"),
                new StationInfo("A"),
                new StationInfo("B"),
                new StationInfo("C"),
                new StationInfo("E"),
            };

            var testLine3 = new StationInfo[]
            {
                new StationInfo("F"),
                new StationInfo("D"),
                new StationInfo("C"),
            };
            MetroLines.Add(new MetroLineInfo("Name1", testLine1));
            MetroLines.Add(new MetroLineInfo("Name2", testLine2));
            MetroLines.Add(new MetroLineInfo("Name3", testLine3));
        }
    }

    /// <summary>
    /// Реализация ветки метро
    /// </summary>
    public class MetroLineInfo
    {
        /// <summary>
        /// Имя ветки метро
        /// </summary>
        public string Name { get; private set; }
        /// <summary>
        /// Какие станции есть в этой ветке
        /// </summary>
        public StationInfo[] Stations { get; private set; }

        public MetroLineInfo(string name, StationInfo[] stations)
        {
            Name = name;
            Stations = stations;
        }

        /// <summary>
        /// Существуют ли станции на этой линии метро
        /// </summary>
        public bool IsContainStations(string firstStation, string lastStation)
        {
            return Stations.Any(item => item.Name == firstStation || item.Name == lastStation);
        }

        /// <summary>
        /// Существуют ли станция на этой линии метро
        /// </summary>
        public bool IsContainStation(string stationName)
        {
            return Stations.Any(item => item.Name == stationName);
        }
    }

    /// <summary>
    /// Реализации станции
    /// </summary>
    public struct StationInfo
    {
        /// <summary>
        /// имя станции
        /// </summary>
        public string Name;

        public StationInfo(string stationName)
        {
            Name = stationName;
        }
    }
}
