﻿using System;
using System.IO;
using System.Text;
using ConsoleApp.Metro;

namespace ConsoleApp
{
    class Program
    {
        static void Main()
        {
            PrintTestMetroMap();
            var metroMap = GetUserMetroMap();

            var firstStation = GetUserStation("укажите начальную станцию метро");
            var lastStation = GetUserStation("укажите конечную станцию метро");
            PrintShortWay(metroMap, firstStation, lastStation);
        }

        private static void PrintTestMetroMap()
        {
            MetroMap testMetroMap = MetroMap.CreateTestMetroMap();
            Console.WriteLine(testMetroMap.GetJson());
        }

        private static string LoadJsonFromFile()
        {
            string path = GetPath();

            try
            {
                using (FileStream fileStream = File.OpenRead(path))
                {
                    byte[] bytesArray = new byte[fileStream.Length];
                    fileStream.Read(bytesArray, 0, bytesArray.Length);
                    string jsonString = Encoding.Default.GetString(bytesArray);
                    return jsonString;
                }
            }
            catch (FileNotFoundException)
            {
                Console.WriteLine("файл не найден");
                return LoadJsonFromFile();
            }
        }

        private static string GetPath()
        {
            string path;
            Console.WriteLine("Нажмите Y чтобы Использовать вшитый JSON");
            var pressedKey = Console.ReadKey();
            if (pressedKey.Key == ConsoleKey.Y)
            {
                string location = AppDomain.CurrentDomain.BaseDirectory;
                path = Path.Combine(location, "JsonFiles", "TestMetro.json");
            }
            else
            {
                Console.WriteLine("укажите путь до JSON файла");
                path = Console.ReadLine();
            }

            return path;
        }

        private static MetroMap GetUserMetroMap()
        {
            string jsonString = LoadJsonFromFile();

            if (!MetroMap.TryGetMetroMapFromJson(jsonString, out MetroMap metroMap))
            {
                return GetUserMetroMap();
            }

            return metroMap;
        }

        private static string GetUserStation(string tipMessage)
        {
            Console.WriteLine(tipMessage);
            string userStation = Console.ReadLine();
            if (string.IsNullOrWhiteSpace(userStation))
            {
                return GetUserStation(tipMessage);
            }

            return userStation.ToUpper();
        }

        private static void PrintShortWay(MetroMap metroMap, string firstStation, string lastStation)
        {
            var shortWay = metroMap.GetShortWay(firstStation, lastStation);
            string shortWayString = string.Empty;

            foreach (var way in shortWay)
            {
                shortWayString += $"{way.Name};";
            }
            Console.WriteLine($"короткий путь:{shortWayString}");
        }
    }
}
