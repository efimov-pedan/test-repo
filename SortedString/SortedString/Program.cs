﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SortedString
{
    class Program
    {
        static void Main(string[] args)
        {
            string userString = Console.ReadLine();
            Console.WriteLine(Result(userString.ToCharArray()));
        }

        private static string Result(char[] userString)
        {
            StringBuilder sb = new StringBuilder();
            List<char> otherChars = new List<char>();

            for (int i = 0; i < userString.Length; i++)
            {
                int nextIndex = i + 1;
                char curChar = userString[i];
                if (nextIndex >= userString.Length)
                {
                    sb.Append(curChar);
                }
                else
                {
                    char nextChar = userString[nextIndex];

                    if (!curChar.Equals(nextChar))
                    {
                        sb.Append(curChar);
                    }
                    else
                    {
                        otherChars.Add(curChar);
                    }
                }
            }

            return TryInsertOtherChars(sb, otherChars);
        }

        private static string TryInsertOtherChars(StringBuilder sortedString, List<char> otherChars, bool lastTry = false)
        {
            for (int i = 0; i < sortedString.Length; i++)
            {
                if (otherChars.Count == 0)
                {
                    return sortedString.ToString();
                }

                int nextIndex = i + 1;
                char curChar = sortedString[i];
                char nextOtherChar = otherChars[0];
                if (nextIndex >= sortedString.Length)
                {
                    if (curChar != nextOtherChar)
                    {
                        if (!nextOtherChar.Equals(curChar))
                        {
                            sortedString.Insert(nextIndex, nextOtherChar);
                            otherChars.Remove(nextOtherChar);
                        }
                    }
                }
                else
                {
                    char nextChar = sortedString[nextIndex];
                    if (!nextOtherChar.Equals(curChar) && !nextOtherChar.Equals(nextChar))
                    {
                        sortedString.Insert(nextIndex, nextOtherChar);
                        otherChars.Remove(nextOtherChar);
                    }
                }
            }

            if (otherChars.Count > 0)
            {
                if (lastTry)
                {
                    return null;
                }
                return TryInsertOtherChars(sortedString, otherChars, true);
            }
            return sortedString.ToString();
        }
    }
}
